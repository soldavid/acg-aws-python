# [A Cloud Guru](https://acloudguru.com/) Discord Discourse

## 2021-07-02

## [ACG Discord Server](https://discord.gg/NwfDnNj54T)

![A Cloud Guru Logo](ACGLogo.png)

# Using Python and Boto3 to Automate AWS Operations

## Friday, July 2nd 16:00 CDT## David Sol

### T-Systems Cloud Architect

![David Sol](DavidSol.jpg)

### Discord: David Sol#9606

### Twitter: [@soldavidcloud](https://twitter.com/soldavidcloud)

### Repository: <https://gitlab.com/soldavid/acg-aws-python>

### Main Notebook: [ACG-AWS.ipynb](ACG-AWS.ipynb)
